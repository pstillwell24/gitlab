# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "GraphQL Pipeline Header", '(JavaScript fixtures)', type: :request, feature_category: :pipeline_composition do
  include ApiHelpers
  include GraphqlHelpers
  include JavaScriptFixturesHelpers

  let_it_be(:namespace) { create(:namespace, name: 'frontend-fixtures') }
  let_it_be(:project) { create(:project, :public, :repository) }
  let_it_be(:user) { project.first_owner }
  let_it_be(:commit) { create(:commit, project: project) }

  let(:query_path) { 'pipelines/graphql/queries/get_pipeline_header_data.query.graphql' }

  context 'with successful pipeline' do
    let_it_be(:pipeline) do
      create(
        :ci_pipeline,
        project: project,
        sha: commit.id,
        ref: 'master',
        user: user,
        status: :success,
        started_at: 1.hour.ago,
        finished_at: Time.current
      )
    end

    it "graphql/pipelines/pipeline_header_success.json" do
      query = get_graphql_query_as_string(query_path)

      post_graphql(query, current_user: user, variables: { fullPath: project.full_path, iid: pipeline.iid })

      expect_graphql_errors_to_be_empty
    end
  end

  context 'with running pipeline' do
    let_it_be(:pipeline) do
      create(
        :ci_pipeline,
        project: project,
        sha: commit.id,
        ref: 'master',
        user: user,
        status: :running,
        created_at: 2.hours.ago,
        started_at: 1.hour.ago
      )
    end

    it "graphql/pipelines/pipeline_header_running.json" do
      query = get_graphql_query_as_string(query_path)

      post_graphql(query, current_user: user, variables: { fullPath: project.full_path, iid: pipeline.iid })

      expect_graphql_errors_to_be_empty
    end
  end
end
